﻿using AWSLambda.Domain.Contracts.AppServices;
using AWSLambda.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AWSLambda.Presentation.API.Controllers
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	public class UsuariosController : ControllerBase
    {
		private readonly IUsuarioAppService _usuarioAppService;

		public UsuariosController(IUsuarioAppService usuarioAppService)
		{
			_usuarioAppService = usuarioAppService;
		}

		[AllowAnonymous]
		[HttpPost("autenticar")]
		public IActionResult Autenticar([FromBody]Usuario usuario)
		{
			try
			{
				var usuarioAutenticado = _usuarioAppService.Autenticar(usuario.Login, usuario.Senha);
				return Ok(usuarioAutenticado);
			}
			catch (Exception ex)
			{
				return BadRequest(new { message = ex.Message });
			}
		}

		[HttpGet]
		public IActionResult ObterTodos()
		{
			var usuarios = _usuarioAppService.ObterTodos();
			return Ok(usuarios);
		}
	}
}