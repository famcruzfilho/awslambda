﻿using AWSLambda.Domain.Contracts.AppServices;
using AWSLambda.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace AWSLambda.Presentation.API.Controllers
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	public class ValuesController : ControllerBase
	{
		private readonly IUsuarioAppService _usuarioAppService;

		public ValuesController(IUsuarioAppService usuarioAppService)
		{
			_usuarioAppService = usuarioAppService;
		}

		[HttpGet]
		public async Task<string> Get()
		{
			var usuario = new Usuario
			{
				UsuarioId = "2",
				Nome = "Fernanda Figueiredo"
			};

			//_usuarioAppService.CriarSQS(usuario);

			var usuarios = await _usuarioAppService.ObterTodosUsuariosSQS();

			foreach(var usr in usuarios)
			{
				_usuarioAppService.RemoverSQS(usr.AWS.RecieptHandle);
			}

			return JsonConvert.SerializeObject(usuarios, Formatting.Indented);
		}
	}
}