﻿using AWSLambda.Domain.Contracts.Services.Base;
using AWSLambda.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSLambda.Domain.Contracts.Services
{
	public interface IUsuarioService : IBaseService<Usuario>, IBaseSQSService<Usuario>
	{
		Task<IEnumerable<Usuario>> ObterTodosUsuariosSQS();
		void RemoverUsuarioSQS(string recieptHandle);
		Usuario Autenticar(string login, string senha);
	}
}