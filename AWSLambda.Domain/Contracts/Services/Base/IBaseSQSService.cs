﻿using Amazon.SQS.Model;
using System.Threading.Tasks;

namespace AWSLambda.Domain.Contracts.Services.Base
{
	public interface IBaseSQSService<TEntity> where TEntity : class
	{
		Task<ReceiveMessageResponse> ObterTodosSQS();
		void RemoverSQS(string recieptHandle);
		void CriarSQS(TEntity obj);
	}
}