﻿using Amazon.SQS.Model;
using AWSLambda.Domain.Contracts.Repository.Base;
using AWSLambda.Domain.Models;
using System.Threading.Tasks;

namespace AWSLambda.Domain.Contracts.Repository
{
	public interface IUsuarioRepository : IBaseRepository<Usuario>, IBaseSQSRepository<Usuario>
	{
		Task<ReceiveMessageResponse> ObterTodosUsuariosSQS();
		void RemoverUsuarioSQS(string recieptHandle);
		Usuario Autenticar(string login, string senha);
	}
}