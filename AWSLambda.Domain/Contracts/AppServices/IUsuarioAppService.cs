﻿using AWSLambda.Domain.Contracts.AppServices.Base;
using AWSLambda.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSLambda.Domain.Contracts.AppServices
{
	public interface IUsuarioAppService : IBaseAppService<Usuario>, IBaseSQSAppService<Usuario>
	{
		Task<IEnumerable<Usuario>> ObterTodosUsuariosSQS();
		void RemoverUsuarioSQS(string recieptHandle);
		Usuario Autenticar(string login, string senha);
	}
}