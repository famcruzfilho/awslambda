﻿using System;
using System.Collections.Generic;

namespace AWSLambda.Domain.Contracts.AppServices.Base
{
	public interface IBaseAppService<TEntity> : IBaseSQSAppService<TEntity>, IDisposable where TEntity : class
	{
		void Criar(TEntity obj);
		void Criar(IEnumerable<TEntity> objs);
		TEntity ObterPorId(int id);
		IEnumerable<TEntity> ObterTodos();
		void Atualizar(TEntity obj);
		void DeletarPorId(int id);
		void Deletar(TEntity obj);
		void Deletar(IEnumerable<TEntity> objs);
	}
}