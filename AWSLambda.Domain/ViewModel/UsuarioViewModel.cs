﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AWSLambda.Domain.ViewModel
{
	public class UsuarioViewModel
	{
		public string UsuarioId { get; set; }
		public string Nome { get; set; }
		public string Login { get; set; }
		public string Senha { get; set; }

		[NotMapped]
		public string Token { get; set; }
	}
}