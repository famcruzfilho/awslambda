﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace AWSLambda.Domain.Models
{
	[Serializable]
	public class Usuario : ISerializable
	{
		public string UsuarioId { get; set; }
		public string Nome { get; set; }
		public string Login { get; set; }
		public string Senha { get; set; }

		[NotMapped]
		public string Token { get; set; }

		[NotMapped]
		public AWS AWS;

		public Usuario()
		{
			SetAWS();
		}

		public Usuario(SerializationInfo info, StreamingContext context)
		{
			SetAWS();
			Login = info.GetString("Login");
			Senha = info.GetString("Senha");
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("UsuarioId", UsuarioId);
			info.AddValue("Nome", Nome);
			info.AddValue("Login", Login);
			info.AddValue("Token", Token);
			info.AddValue("AWS", AWS);
		}

		protected void SetAWS()
		{
			AWS = new AWS
			{
				ServiceURL = "https://sqs.us-east-1.amazonaws.com",
				AccessKeyId = "AKIA2VOENDTQK3DQEHVS",
				SecretAccessKey = "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",
				QueueURL = "https://sqs.us-east-1.amazonaws.com/733240696032/Usuarios"
            };
		}

		public AWS GetAWS()
		{
			return AWS;
		}
	}
}