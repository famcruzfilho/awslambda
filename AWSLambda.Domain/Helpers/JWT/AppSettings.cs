﻿namespace AWSLambda.Domain.Helpers.JWT
{
	public class AppSettings
	{
		public string Secret { get; set; }
	}
}