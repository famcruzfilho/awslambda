﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AWSLambda.Infraestructure.Migrations
{
    public partial class Change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReceiptHandle",
                table: "Usuarios");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ReceiptHandle",
                table: "Usuarios",
                maxLength: 250,
                nullable: false,
                defaultValue: "");
        }
    }
}