﻿using Amazon.SQS.Model;
using AWSLambda.Domain.Contracts.Repository;
using AWSLambda.Domain.Models;
using AWSLambda.Infraestructure.Data.Context;
using AWSLambda.Infraestructure.Data.Repositories.Base;
using System.Linq;
using System.Threading.Tasks;

namespace AWSLambda.Infraestructure.Data.Repositories
{
	public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
	{
		private readonly DataContext _dataContext;
		private readonly AWSContext<Usuario> _AWSContext;

		public UsuarioRepository(DataContext context, AWSContext<Usuario> AWSContext) : base(context, AWSContext)
		{
			_dataContext = context;
			_AWSContext = AWSContext;
		}

		public Usuario Autenticar(string login, string senha)
		{
			var usuario = _dataContext.Usuarios.FirstOrDefault(x => x.Login == login && x.Senha == senha);
			return usuario;
		}

		public async Task<ReceiveMessageResponse> ObterTodosUsuariosSQS()
		{
			return await _AWSContext.ObterTodosSQS();
		}

		public void RemoverUsuarioSQS(string recieptHandle)
		{
			_AWSContext.RemoverSQS(recieptHandle);
		}
	}
}