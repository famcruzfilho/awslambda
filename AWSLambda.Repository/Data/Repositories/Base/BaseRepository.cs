﻿using Amazon.SQS.Model;
using AWSLambda.Domain.Contracts.Repository.Base;
using AWSLambda.Infraestructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSLambda.Infraestructure.Data.Repositories.Base
{
	public class BaseRepository<TEntity> : IBaseRepository<TEntity>, IBaseSQSRepository<TEntity>, IDisposable where TEntity : class
	{
		private readonly DataContext _dataContext;
		private readonly AWSContext<TEntity> _AWSContext;

		public BaseRepository(DataContext dataContext, AWSContext<TEntity> AWSContext)
		{
			_dataContext = dataContext;
			_AWSContext = AWSContext;
		}

		public void Atualizar(TEntity obj)
		{
			_dataContext.Entry(obj).State = EntityState.Modified;
			_dataContext.SaveChanges();
		}

		public void Criar(TEntity obj)
		{
			_dataContext.Set<TEntity>().Add(obj);
			_dataContext.SaveChanges();
		}

		public void Criar(IEnumerable<TEntity> objs)
		{
			foreach (TEntity obj in objs)
			{
				_dataContext.Set<TEntity>().Add(obj);
				_dataContext.SaveChanges();
			}
		}

		public void DeletarPorId(int id)
		{
			var obj = _dataContext.Set<TEntity>().Find(id);
			_dataContext.Set<TEntity>().Remove(obj);
			_dataContext.SaveChanges();
		}

		public void Deletar(TEntity obj)
		{
			_dataContext.Set<TEntity>().Remove(obj);
			_dataContext.SaveChanges();
		}

		public void Deletar(IEnumerable<TEntity> objs)
		{
			foreach (TEntity obj in objs)
			{
				_dataContext.Set<TEntity>().Remove(obj);
				_dataContext.SaveChanges();
			}
		}

		public TEntity ObterPorId(int id)
		{
			return _dataContext.Set<TEntity>().Find(id);
		}

		public IEnumerable<TEntity> ObterTodos()
		{
			return _dataContext.Set<TEntity>();
		}

		public void Dispose()
		{
			_dataContext.Dispose();
		}

		public async Task<ReceiveMessageResponse> ObterTodosSQS()
		{
			return await _AWSContext.ObterTodosSQS();
		}

		public void RemoverSQS(string recieptHandle)
		{
			_AWSContext.RemoverSQS(recieptHandle);
		}

		public void CriarSQS(TEntity obj)
		{
			_AWSContext.CriarSQS(obj);
		}
	}
}