﻿using Amazon;
using Amazon.Runtime;
using Amazon.SQS;
using Amazon.SQS.Model;
using AWSLambda.Domain.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace AWSLambda.Infraestructure.Data.Context
{
	public class AWSContext<TEntity> where TEntity : class
	{
		public AmazonSQSConfig AmazonSQSConfig { get; set; }
		public BasicAWSCredentials BasicAWSCredentials { get; set; }
		public AmazonSQSClient AmazonSQSClient { get; set; }
		public SendMessageRequest SendMessageRequest { get; set; }
		public ReceiveMessageRequest ReceiveMessageRequest { get; set; }
		public ReceiveMessageResponse ReceiveMessageResponse { get; set; }
		public SendMessageResponse SendMessageResponse { get; set; }
		public DeleteMessageRequest DeleteMessageRequest { get; set; }
		public DeleteMessageResponse DeleteMessageResponse { get; set; }

		public AWSContext(AWS AWS)
		{
			AmazonSQSConfig = new AmazonSQSConfig();
			AmazonSQSConfig.ServiceURL = AWS.ServiceURL;
			BasicAWSCredentials = new BasicAWSCredentials(AWS.AccessKeyId, AWS.SecretAccessKey);
			AmazonSQSClient = new AmazonSQSClient(BasicAWSCredentials, RegionEndpoint.USEast1);
			SendMessageRequest = new SendMessageRequest();
			SendMessageRequest.QueueUrl = AWS.QueueURL;
			ReceiveMessageRequest = new ReceiveMessageRequest();
			ReceiveMessageRequest.QueueUrl = AWS.QueueURL;
			DeleteMessageRequest = new DeleteMessageRequest();
			DeleteMessageRequest.QueueUrl = AWS.QueueURL;
		}

		public async void CriarSQS(TEntity entity)
		{
			try
			{
				SendMessageRequest.MessageBody = JsonConvert.SerializeObject(entity);
				SendMessageResponse = await AmazonSQSClient.SendMessageAsync(SendMessageRequest);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public async Task<ReceiveMessageResponse> ObterTodosSQS()
		{
            ReceiveMessageRequest.MaxNumberOfMessages = 10;
			ReceiveMessageResponse = await AmazonSQSClient.ReceiveMessageAsync(ReceiveMessageRequest);

			return ReceiveMessageResponse;
		}

		public async void RemoverSQS(string recieptHandle)
		{
			DeleteMessageRequest.ReceiptHandle = recieptHandle;
			DeleteMessageResponse = await AmazonSQSClient.DeleteMessageAsync(DeleteMessageRequest);
		}
	}
}