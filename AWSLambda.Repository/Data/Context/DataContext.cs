﻿using AWSLambda.Domain.Models;
using AWSLambda.Infraestructure.Data.Mappings;
using Microsoft.EntityFrameworkCore;

namespace AWSLambda.Infraestructure.Data.Context
{
	public class DataContext : DbContext
	{
		public DataContext()
		{
		}

		public DataContext(DbContextOptions<DataContext> options) : base(options)
		{
		}

		public DbSet<Usuario> Usuarios { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
		{
			if (!optionBuilder.IsConfigured)
			{
				optionBuilder.UseSqlServer(connectionString: "Data Source=localhost;Initial Catalog=AWSLambda;User Id=sa;Password=Em0t10n%");
			}
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//Mappings
			modelBuilder.ApplyConfiguration(new UsuarioMap());
		}
	}
}