﻿using AWSLambda.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AWSLambda.Infraestructure.Data.Mappings
{
	public class UsuarioMap : IEntityTypeConfiguration<Usuario>
	{
		public void Configure(EntityTypeBuilder<Usuario> builder)
		{
			builder.HasKey(x => x.UsuarioId);

			builder.Property(x => x.Nome)
				.IsRequired()
				.HasMaxLength(250);

			builder.Property(x => x.Login)
				.IsRequired()
				.HasMaxLength(250);

			builder.Property(x => x.Senha)
				.IsRequired()
				.HasMaxLength(250);
		}
	}
}