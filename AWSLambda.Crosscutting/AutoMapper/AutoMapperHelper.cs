﻿using AutoMapper;

namespace AWSLambda.Crosscutting.AutoMapper
{
	public abstract class AutoMapperHelper
	{
		public static MapperConfiguration CreateConfiguration()
		{
			return new MapperConfiguration(cfg =>
			{
				cfg.AddProfile(new AutoMapperProfile());
			});
		}
	}
}