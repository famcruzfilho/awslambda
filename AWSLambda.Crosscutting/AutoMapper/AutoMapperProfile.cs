﻿using AutoMapper;
using AWSLambda.Domain.DTOs;
using AWSLambda.Domain.Models;
using AWSLambda.Domain.ViewModel;

namespace AWSLambda.Crosscutting.AutoMapper
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			AutoMapperFilters();
			AutoMapperEntities();
			AutoMapperEntityToModel();
			AutoMapperCustom();
		}

		/// <summary>
		/// AutoMapperFilters
		/// </summary>
		private void AutoMapperFilters()
		{
			//Models para DTO's and vice versa
			CreateMap<Usuario, UsuarioDTO>().ReverseMap();
		}

		/// <summary>
		/// AutoMapperEntities
		/// </summary>
		private void AutoMapperEntities()
		{

		}

		/// <summary>
		/// AutoMapperEntityToModel
		/// </summary>
		private void AutoMapperEntityToModel()
		{
			CreateMap<Usuario, UsuarioViewModel>().ReverseMap();
		}

		/// <summary>
		/// AutoMapperCustom
		/// </summary>
		private void AutoMapperCustom()
		{
			
		}
	}
}