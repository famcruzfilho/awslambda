﻿using AWSLambda.ApplicationService.AppServices;
using AWSLambda.Business.Services;
using AWSLambda.Domain.Contracts.AppServices;
using AWSLambda.Domain.Contracts.Repository;
using AWSLambda.Domain.Contracts.Services;
using AWSLambda.Domain.Models;
using AWSLambda.Infraestructure.Data.Context;
using AWSLambda.Infraestructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AWSLambda.Crosscutting.IoC
{
	public static class DependencyInjection
	{
		public static IServiceCollection RegisterClass(IServiceCollection services, string stringConexao)
		{
			#region [ Repositories ]
			services.AddTransient<IUsuarioRepository, UsuarioRepository>();
			#endregion

			#region [ Services ]
			services.AddTransient<IUsuarioService, UsuarioService>();
			#endregion

			#region [ Application Services ]
			services.AddTransient<IUsuarioAppService, UsuarioAppService>();
			#endregion

			#region [ AWSContext ]
			services.AddTransient<AWSContext<Usuario>>(s => new AWSContext<Usuario>(new Usuario().AWS));
			#endregion

			return services.AddDbContext<DataContext>(options => options.UseSqlServer(stringConexao));
		}
	}
}