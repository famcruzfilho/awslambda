﻿using AWSLambda.Business.Services.Base;
using AWSLambda.Domain.Contracts.Repository;
using AWSLambda.Domain.Contracts.Services;
using AWSLambda.Domain.Helpers.JWT;
using AWSLambda.Domain.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AWSLambda.Business.Services
{
	public class UsuarioService : BaseService<Usuario>, IUsuarioService
	{
		private readonly IUsuarioRepository _usuarioRepository;
		private readonly AppSettings _appSettings;

		public UsuarioService(IUsuarioRepository usuarioRepository, IOptions<AppSettings> appSettings) : base(usuarioRepository)
		{
			_usuarioRepository = usuarioRepository;
			_appSettings = appSettings.Value;
		}

		public Usuario Autenticar(string login, string senha)
		{
			var usuario = _usuarioRepository.Autenticar(login, senha);
			if (usuario == null)
			{
				throw new Exception("Usuário não autorizado");
			}
			var tokenHandler = new JwtSecurityTokenHandler();
			var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(new Claim[]
				{
					new Claim(ClaimTypes.Name, usuario.UsuarioId.ToString())
				}),
				Expires = DateTime.UtcNow.AddMinutes(5),
				SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
			};
			var token = tokenHandler.CreateToken(tokenDescriptor);
			usuario.Token = tokenHandler.WriteToken(token);
			usuario.Senha = null;

			return usuario;
		}

		public async Task<IEnumerable<Usuario>> ObterTodosUsuariosSQS()
		{
			var receiveMessageResponse = await _usuarioRepository.ObterTodosUsuariosSQS();
			var _usuarios = new List<Usuario>();
			if (receiveMessageResponse.Messages.Count > 0)
			{
				foreach (var message in receiveMessageResponse.Messages)
				{
					var usuario = JsonConvert.DeserializeObject<Usuario>(message.Body);
					usuario.UsuarioId = message.MessageId;
					usuario.AWS.RecieptHandle = message.ReceiptHandle;
					_usuarios.Add(usuario);
				}
			}

			return _usuarios;
		}

		public void RemoverUsuarioSQS(string recieptHandle)
		{
			_usuarioRepository.RemoverUsuarioSQS(recieptHandle);
		}
	}
}