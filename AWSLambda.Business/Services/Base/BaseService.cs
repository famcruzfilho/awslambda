﻿using Amazon.SQS.Model;
using AWSLambda.Domain.Contracts.Repository.Base;
using AWSLambda.Domain.Contracts.Services.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSLambda.Business.Services.Base
{
	public class BaseService<TEntity> : IBaseService<TEntity>, IBaseSQSService<TEntity> where TEntity : class
	{
		private readonly IBaseRepository<TEntity> _baseRepository;
		private readonly IBaseSQSRepository<TEntity> _baseSQSRepository;

		public BaseService(IBaseRepository<TEntity> baseRepository)
		{
			_baseRepository = baseRepository;
			_baseSQSRepository = baseRepository;
		}

		public void Atualizar(TEntity obj)
		{
			_baseRepository.Atualizar(obj);
		}

		public void Criar(TEntity obj)
		{
			_baseRepository.Criar(obj);
		}

		public void Criar(IEnumerable<TEntity> objs)
		{
			_baseRepository.Criar(objs);
		}

		public void CriarSQS(TEntity obj)
		{
			_baseSQSRepository.CriarSQS(obj);
		}

		public void Deletar(TEntity obj)
		{
			_baseRepository.Deletar(obj);
		}

		public void Deletar(IEnumerable<TEntity> objs)
		{
			_baseRepository.Deletar(objs);
		}

		public void DeletarPorId(int id)
		{
			_baseRepository.DeletarPorId(id);
		}

		public void Dispose()
		{
			_baseRepository.Dispose();
		}

		public TEntity ObterPorId(int id)
		{
			return _baseRepository.ObterPorId(id);
		}

		public IEnumerable<TEntity> ObterTodos()
		{
			return _baseRepository.ObterTodos();
		}

		public async Task<ReceiveMessageResponse> ObterTodosSQS()
		{
			return await _baseSQSRepository.ObterTodosSQS();
		}

		public void RemoverSQS(string recieptHandle)
		{
			_baseSQSRepository.RemoverSQS(recieptHandle);
		}
	}
}