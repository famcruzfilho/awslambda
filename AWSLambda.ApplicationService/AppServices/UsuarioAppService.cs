﻿using AWSLambda.ApplicationService.AppServices.Base;
using AWSLambda.Domain.Contracts.AppServices;
using AWSLambda.Domain.Contracts.Services;
using AWSLambda.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSLambda.ApplicationService.AppServices
{
	public class UsuarioAppService : BaseAppService<Usuario>, IUsuarioAppService
	{
		private readonly IUsuarioService _usuarioService;

		public UsuarioAppService(IUsuarioService usuarioService) : base(usuarioService)
		{
			_usuarioService = usuarioService;
		}

		public Usuario Autenticar(string login, string senha)
		{
			return _usuarioService.Autenticar(login, senha);
		}

		public Task<IEnumerable<Usuario>> ObterTodosUsuariosSQS()
		{
			return _usuarioService.ObterTodosUsuariosSQS();
		}

		public void RemoverUsuarioSQS(string recieptHandle)
		{
			_usuarioService.RemoverUsuarioSQS(recieptHandle);
		}
	}
}