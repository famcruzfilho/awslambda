﻿using Amazon.SQS.Model;
using AWSLambda.Domain.Contracts.AppServices.Base;
using AWSLambda.Domain.Contracts.Services.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSLambda.ApplicationService.AppServices.Base
{
	public class BaseAppService<TEntity> : IBaseAppService<TEntity>, IBaseSQSAppService<TEntity> where TEntity : class
	{
		private readonly IBaseService<TEntity> _baseService;
		private readonly IBaseSQSService<TEntity> _baseSQSService;

		public BaseAppService(IBaseService<TEntity> baseService)
		{
			_baseService = baseService;
			_baseSQSService = baseService;
		}

		public void Atualizar(TEntity obj)
		{
			_baseService.Atualizar(obj);
		}

		public void Criar(TEntity obj)
		{
			_baseService.Criar(obj);
		}

		public void Criar(IEnumerable<TEntity> objs)
		{
			_baseService.Criar(objs);
		}

		public void CriarSQS(TEntity obj)
		{
			_baseSQSService.CriarSQS(obj);
		}

		public void Deletar(TEntity obj)
		{
			_baseService.Deletar(obj);
		}

		public void Deletar(IEnumerable<TEntity> objs)
		{
			_baseService.Deletar(objs);
		}

		public void DeletarPorId(int id)
		{
			_baseService.DeletarPorId(id);
		}

		public void Dispose()
		{
			_baseService.Dispose();
		}

		public TEntity ObterPorId(int id)
		{
			return _baseService.ObterPorId(id);
		}

		public IEnumerable<TEntity> ObterTodos()
		{
			return _baseService.ObterTodos();
		}

		public async Task<ReceiveMessageResponse> ObterTodosSQS()
		{
			return await _baseSQSService.ObterTodosSQS();
		}

		public void RemoverSQS(string recieptHandle)
		{
			_baseSQSService.RemoverSQS(recieptHandle);
		}
	}
}